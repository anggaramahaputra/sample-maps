import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapsDummyComponent } from './maps-dummy/maps-dummy.component';
import { HttpClientModule} from  '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    MapsDummyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
