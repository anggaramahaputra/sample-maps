import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-maps-dummy',
  templateUrl: './maps-dummy.component.html',
  styleUrls: ['./maps-dummy.component.scss']
})
export class MapsDummyComponent implements OnInit, AfterViewInit  {
  private map;
  constructor(private http: HttpClient) { }
  options : any = {};
  ngOnInit() {
   
  }
  ngAfterViewInit(){
    this.InitMap();
  }

  private InitMap() : void {

    let DefaultIcon = L.icon({
      iconRetinaUrl : 'assets/marker-icon-2x.png',
      iconUrl : 'assets/marker-icon.png',
      shadowUrl : 'assets/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41],
    });

    L.Marker.prototype.options.icon = DefaultIcon;

    this.map = L.map('map', {
      center: [-6.261493 , 106.810600 ],
      zoom: 11,
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 10, 
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
    L.marker([-6.225014 , 106.900447]).addTo(this.map).bindPopup('Jakarta Timur : ' + 28 ,{autoClose:false}).openPopup();
    L.marker([-6.261493 , 106.810600]).addTo(this.map).bindPopup('Jakarta Selatan : ' + 52 ,{autoClose:false}).openPopup();
    L.marker([-6.168329 , 106.758849]).addTo(this.map).bindPopup('Jakarta Barat : ' + 32 ,{autoClose:false}).openPopup();
    L.marker([-6.138414 , 106.863956]).addTo(this.map).bindPopup('Jakarta Utara : ' + 60 ,{autoClose:false}).openPopup();
    
    this.http.get('assets/data/boundaries.geojson').subscribe((boundaries:String) =>{
      console.log(boundaries);
      const	geojson = L.geoJson(boundaries).addTo(this.map);

	    // geojson.eachLayer(function (layer) {
		  //   layer.bindPopup(layer.feature.properties.NAME_2);
	    // });
    });
  }

}
