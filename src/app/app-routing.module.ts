import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapsDummyComponent } from './maps-dummy/maps-dummy.component';


const routes: Routes = [
  {
    path: "",
    component: MapsDummyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
